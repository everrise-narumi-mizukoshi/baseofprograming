package algorithm;

public class MinSecond {
    public static void main(String[] args) {
        int[] numbers = {41, 22, 35, 40, 25, 16, 7, 38, 9, 10, 14, 27, 32, 46, 53};
        int numMin = getNumMinValue(numbers, 2);
        
        System.out.println("小さい方から2番目の値が入っているのは" + getNumMinIndex(numbers, numMin) + "番目の"
                + numMin + "です");
    }
    
    /**
     * 最小からn番目の数を返す.
     * @param nums 配列
     * @return 2番目に小さい数
     */
    public static int getNumMinValue(int[] nums, int num) {
        int[] sortNums = nums.clone();
        getBubbleSort(sortNums);
        return sortNums[num - 1];
    }
    
    /**
     * 値のindexを取得.
     * @param nums 配列
     * @param numMin 値
     * @return 値のindex
     */
    public static int getNumMinIndex(int[] nums, int numMin) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == numMin) {
                index = i;
            }
        }
        return index;
    }
    
    /**
     * 配列を昇順にソートして返す.
     * @param list 配列
     * @return 昇順に並べ替えた配列
     */
    public static int[] getBubbleSort(int[] list) {
        int len = list.length;
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (list[i] > list[j]) {
                    int temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                }
            }
        }
        return list;
    }


}
