package algorithm;

import java.util.Arrays;

public class NumberSort {

    public static void main(String[] args) {
        int[] nums = {5, 2, 9, 1};
        String[] ineSigns = {"<", ">", "<"};
        System.out.println(Arrays.toString(numberSort(nums, ineSigns)));

    }
    
    public static int[] numberSort(int[] nums, String[] inequalitySigns) {
        int[] resultNumSort = new int[nums.length];
        // numsを昇順にソート
        Arrays.sort(nums);
        
        // 左の不等号から順に評価する
        int minCnt = 0;
        int maxCnt = 0;
        for (int i = 0; i < inequalitySigns.length; i++) {
            String inequalitySign = inequalitySigns[i];
            if (">".equals(inequalitySign)) {
                resultNumSort[i] = nums[nums.length - (1 + maxCnt)];
                maxCnt++;
            } else if ("<".equals(inequalitySign)) {
                resultNumSort[i] = nums[minCnt];
                minCnt++;
            }
        }
        resultNumSort[resultNumSort.length - 1] = nums[nums.length - (1 + maxCnt)];
        return resultNumSort;
    }

}
