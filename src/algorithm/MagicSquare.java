package algorithm;

public class MagicSquare {

    public static void main(String[] args) {
        printMagicSquare(3);
        
        printMagicSquare(5);

    }
    
    public static void printMagicSquare(int num) {
        if (num == 0) {
            return;
        }
        int[][] nums = new int[num][num];
        int x,y,xw,yw;

        y = num - 1;
        x = num / 2;
        for (int i = 1; i <= num * num; i++) {
            nums[y][x] = i;
            yw = (y + 1) % num;
            xw = (x + 1) % num;
            if (nums[yw][xw] != 0) {
                yw = (y + (num - 1)) % num;
                xw = x;
            }
            y = yw;
            x = xw;
        }
        for (y = 0; y < num; y++) {
            for (x = 0; x < num; x++) {
                if (nums[y][x] > 9) {
                    System.out.print(nums[y][x] + "  ");
                } else {
                    System.out.print(" " + nums[y][x] + "  ");
                }
            }
            System.out.println("");
        }
    }

}
