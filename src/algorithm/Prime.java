package algorithm;

import java.util.ArrayList;

public class Prime {

    public static void main(String[] args) {
        System.out.println(getPrime());
    }
    
    /**
     * 素数を返す.
     * @return 100以下の素数のリストを返す.
     */
    public static String getPrime() {
        ArrayList<Integer> primes = new ArrayList<Integer>();
        for (int i = 1; i <= 100; i++) {
            // 素数：1と自分自身以外では割れない数
            int divisor = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    divisor++;
                }
            }
            if (divisor == 2) {
                primes.add(i);
            }
        }
        return primes.toString();
    }

}
