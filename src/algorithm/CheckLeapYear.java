package algorithm;

public class CheckLeapYear {

    public static void main(String[] args) {
        System.out.println("2016年は");
        System.out.println(checkLeapYear(2016) ? "うるう年" : "うるう年ではない");
        
        System.out.println("2000年は");
        System.out.println(checkLeapYear(2000) ? "うるう年" : "うるう年ではない");
        
        System.out.println("1900年は");
        System.out.println(checkLeapYear(1900) ? "うるう年" : "うるう年ではない");

    }
    
    public static boolean checkLeapYear(int year) {
        // 西暦が4で割り切れる年はうるう年
        // ただし、4で割り切れても100で割り切れる年はうるう年でない
        // ただし、100で割り切れても400で割り切れる年はうるう年
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }

}
