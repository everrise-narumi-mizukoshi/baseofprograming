package algorithm;

public class MaxMin {

    public static void main(String[] args) {
        System.out.println("最大値：" + getMax(100, 200, 300, 250));
        System.out.println("最小値：" + getMin(100, 200, 300, 250));
    }
    
    /**
     * 最小値を取得.
     * @param numA 数字１
     * @param numB 数字２
     * @param numC 数字３
     * @param numD 数字４
     * @return 最小値
     */
    public static int getMin(int numA, int numB, int numC, int numD) {
        int min = numA;
        
        if (min >= numB) {
            min = numB;
        }
        if (min >= numC) {
            min = numC;
        }
        if (min >= numD) {
            min = numD;
        }
        return min;
    }
    
    /**
     * 最大値を返す.
     * @param numA 数字1
     * @param numB 数字2
     * @param numC 数字3
     * @param numD 数字4
     * @return 最大値
     */
    public static int getMax(int numA, int numB, int numC, int numD) {
        int[] nums = {numA, numB, numC, numD};
        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            if (max <= nums[i]) {
                max = nums[i];
            }
        }
        return max;
    }

}
