package algorithm;

public class Calculator {
    /**
     * 簡易電卓.
     */
    public static void main(String[] args) {
        System.out.println(calculation("+", 5, 8) == null ? "計算できませんでした" : calculation("+", 5, 8));
        System.out.println(calculation("-", 5, 8) == null ? "計算できませんでした" : calculation("-", 5, 8));
        System.out.println(calculation("*", 5, 8) == null ? "計算できませんでした" : calculation("*", 5, 8));
        System.out.println(calculation("/", 5, 8) == null ? "計算できませんでした" : calculation("/", 40, 8));
        System.out.println(calculation("/", 5, 0) == null ? "計算できませんでした" : calculation("/", 5, 0));
        System.out.println(calculation("%", 5, 8) == null ? "計算できませんでした" : calculation("%", 5, 8));
    }
    
    /**
     * 計算結果を返す.
     * @param operator 演算子
     * @param numA 数字１
     * @param numB 数字２
     * @return 計算結果
     */
    public static Integer calculation(String operator, int numA, int numB) {
        Integer calcResult = null;
        switch (operator) {
        case "+":
            calcResult = numA + numB;
            break;
        case "-":
            calcResult = numA - numB;
            break;
        case "*":
            calcResult = numA * numB;
            break;
        case "/":
            if (numB == 0) {
                break;
            }
            calcResult = numA / numB;
            break;
        case "%":
            if (numB == 0) {
                break;
            }
            calcResult = numA % numB;
            break;
        default:
            break;
        }
        return calcResult;
    }

}
