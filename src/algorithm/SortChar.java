package algorithm;

public class SortChar {

    public static void main(String[] args) {
        String[] charList = {"G", "I", "N", "R", "T", "U"};
        System.out.println("[G,I,N,R,T,U]の並びの総数：" + getAllPattern(charList));
    }
    
    public static int getAllPattern(String[] charList) {
        if (charList == null) {
            return 0;
        }
        int len = charList.length;
        int allPattern = 1;
        for (int i = 1; i <= len; i++) {
            allPattern *= i;
        }
        return allPattern;
    }

}
