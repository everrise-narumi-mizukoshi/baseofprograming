package algorithm;

/**
 * 最大公約数（ユーグリッドの互除法.）
 * @author narumi
 *
 */
public class GreatestCommonDivisor {

    /**
     * 最大公約数を取得.
     * @param args コマンド引数
     */
    public static void main(String[] args) {
        int a = 0, b = 0, r;
        if (args != null && args.length == 2) {
            a = Integer.valueOf(args[0]);
            b = Integer.valueOf(args[1]);
        } else {
            System.out.println("引数が足りません。引数は数字2つです。");
            return;
        }
        //ユークリッドの互除法により最大公約数を求める
        while ((r = a % b) != 0) {
            a = b;
            b = r;
        }
        System.out.println( "最大公約数は " + b );
    }

}
