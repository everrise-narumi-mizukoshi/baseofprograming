package algorithm;

public class GreatestCommonDivisor2 {

    public static void main(String[] args) {
        System.out.println(getGreatestComDivisor(17, 54));
        System.out.println(getGreatestComDivisor(3, 54));
    }
    
    /**
     * 2つの引数の最大公約数を返す(ユーグリッドの互除法を使用しない).
     * @param numA 引数１
     * @param numB 引数２
     * @return 最大公約数
     */
    public static int getGreatestComDivisor(int numA, int numB) {
        int max = 0;
        if (numA <= numB) {
            max = numA;
        } else {
            max = numB;
        }
        int gcd = 1;
        for (int i = 1; i <= max; i++) {
            if (numA % i == 0 && numB % i == 0) {
                gcd = i;
            }
        }
        return gcd;
    }

}
