package algorithm;

import java.util.Arrays;

public class QuickSort {

    public static void main(String[] args) {
        int[] nums = {41, 22, 35, 40, 25, 16, 7, 38, 9, 10, 14, 27, 32, 46, 53};
        System.out.println(Arrays.toString(getQuickSort(nums, 0, nums.length - 1)));

    }
    
    /**
     * 配列listのleftからrightまでの間のデータ列をクイックソートする.
     * @param list 配列
     * @param left 左のindex
     * @param right 右のindex
     * @return 昇順に並べ替えた配列
     */
    public static int[] getQuickSort(int[] list, int left, int right) {
        if (left >= right) {
            return list;
        }
        int axisNum = list[(left + right) / 2];
        int l = left;
        int r = right;
        int tmp;
        while (l <= r) {
            while (list[l] < axisNum) {
                l++;
            }
            while (list[r] > axisNum) {
                r--;
            }
            if (l <= r) {
                tmp = list[l];
                list[l] = list[r];
                list[r] = tmp;
                l++;
                r--;
            }
        }
        getQuickSort(list, left, r);  // ピボットより左側をクイックソート
        getQuickSort(list, l, right); // ピボットより右側をクイックソート
        
        return list;
    }

}
