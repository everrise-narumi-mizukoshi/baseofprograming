package algorithm;

import java.util.Arrays;
import java.util.Collections;

public class RandomSort {

    public static void main(String[] args) {
        String[] cards = {
                "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "d10", "dj", "dq", "dk",
                "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "sj", "sq", "sk",
                "h1", "h2", "h3", "h4", "h5", "h6", "h7", "h8", "h9", "h10", "hj", "hq", "hk",
                "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "cj", "cq", "ck"};
        
        String[] cards2 = {
                "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "d10", "dj", "dq", "dk",
                "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "sj", "sq", "sk",
                "h1", "h2", "h3", "h4", "h5", "h6", "h7", "h8", "h9", "h10", "hj", "hq", "hk",
                "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "cj", "cq", "ck"};

        System.out.println(Arrays.toString(getRandomSort(cards)));
       
        System.out.println(Arrays.toString(getRandomSortSelf(cards2)));
        
    }
    
    /**
     * 配列をランダムに並び替えて返す.
     * @param list 配列
     * @return ランダムに並び替えた配列
     */
    public static String[] getRandomSort(String[] list) {
        Collections.shuffle(Arrays.asList(list));
        return list;
    }
    
    /**
     * 配列をランダムに並び替えて返す.
     * @param list 配列
     * @return ランダムに並び替えた配列
     */
    public static String[] getRandomSortSelf(String[] list) {
        for (int i = 0; i < list.length; i++) {
            int t = (int)(Math.random() * i);
            String temp = list[i];
            list[i] = list[t];
            list[t] = temp;
        }
        
        return list;
    }
    
}
