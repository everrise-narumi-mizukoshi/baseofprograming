package algorithm;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        int[] nums = {41, 22, 35, 40, 25, 16, 7, 38, 9, 10, 14, 27, 32, 46, 53};
        System.out.println(Arrays.toString(getBubbleSort(nums)));

    }
    
    /**
     * 配列を昇順にソートして返す.
     * @param list 配列
     * @return 昇順に並べ替えた配列
     */
    public static int[] getBubbleSort(int[] list) {
        int len = list.length;
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (list[i] > list[j]) {
                    int temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                }
            }
        }
        return list;
    }

}
