package function;

public class Kusa {
    public static void main(String[] args) {
        String kusa = kusa(10);
        System.out.println(kusa);
    }
    
    /**
     * 引数の数だけ草を生やす.
     * @param kusaNum 草の数.
     * @return 草（w）が生えた文字列.
     */
    public static String kusa(int kusaNum) {
        String kusa = "";
        for (int i = 0; i < kusaNum; i++) {
            kusa += "w";
        }
        return kusa;
    }

}
