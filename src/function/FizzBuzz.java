package function;

public class FizzBuzz {

    public static void main(String[] args) {
        System.out.println(getFizzBuzzStr(30));
    }
    
    /**
     * 指定された数までのFizzBuzzの結果を返す.
     * @param num 指定数
     * @return スペース区切りの文字列
     */
    public static String getFizzBuzzStr(int num) {
        String fzBzStr = "";
        for (int i = 1; i <= num; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                fzBzStr += "FizzBuzz";
            } else if (i % 3 == 0) {
                fzBzStr += "Fizz";
            } else if (i % 5 == 0) {
                fzBzStr += "Buzz";
            } else {
                fzBzStr += i;
            }
            fzBzStr += " ";
        }
        return fzBzStr;
    }

}
