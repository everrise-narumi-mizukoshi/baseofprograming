package function;

public class Argument1 {

    /**
     * 引数１.
     */
    public static void main(String[] args) {
        
        String voice = hello("hogehoge");
        System.out.println(voice);
    }
    
    public static String hello(String name) {
        return "hello " + name;
    }

}
