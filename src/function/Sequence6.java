package function;

public class Sequence6 {

    public static void main(String[] args) {
        int result = getSeqSum(3, 8, 3);
        System.out.println(result);
    }
    
    /**
     * 等差数列の結果を返す.
     * @param firstNum 初項
     * @param diffNum 公差
     * @param seqNum 指定項
     * @return 指定項の数
     */
    public static int getSeqNumsComDiff(int firstNum, int diffNum, int seqNum) {
         return firstNum + diffNum * (seqNum - 1);
    }
    
    /**
     * 等差数列の指定項までの和を返す.
     * @param firstNum 初項
     * @param diffNum 公差
     * @param seqNum 指定項
     * @return 合計値
     */
    public static int getSeqSum(int firstNum, int diffNum, int seqNum) {
        int sum = 0;
        for (int i = 1; i <= seqNum; i++) {
            sum += getSeqNumsComDiff(firstNum, diffNum, seqNum);
        }
        return sum;
    }

}
