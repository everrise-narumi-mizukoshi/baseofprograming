package function;

import java.util.ArrayList;

public class Sequence4 {

    public static void main(String[] args) {
        ArrayList<Integer> result = getAllSeqFirst5Diff3LteAsArray(25);
        System.out.println(result.toString());
    }
    
    /**
     * 初項5, 公差3の等差数列の指定した数以下のすべての項を配列で返す.
     * @param num 指定数.
     * @return 指定数以下のdすべての数の配列.
     */
    public static ArrayList<Integer> getAllSeqFirst5Diff3LteAsArray(int num) {
        ArrayList<Integer> allSeqResults = new ArrayList<Integer>();
        for (int i = 1; getSeqFirst5Diff3ByIndex(i) <= num; i++) {
            allSeqResults.add(getSeqFirst5Diff3ByIndex(i));
        }
         return allSeqResults;
    }
    
    /**
     * 初項5, 公差3の等差数列の指定項の数を返す.
     * @param seqtionNum 指定項.
     * @return 指定項の数.
     */
    public static int getSeqFirst5Diff3ByIndex(int seqtionNum) {
         return 5 + 3 * (seqtionNum - 1);
    }

}
