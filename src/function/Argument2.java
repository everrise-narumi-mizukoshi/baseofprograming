package function;

public class Argument2 {
    /**
     * 引数２.
     */
    public static void main(String[] args) {
        String voice = nameAndName("hogehoge", "piyopiyo");
        System.out.println(voice);
    }
    
    public static String nameAndName(String name1, String name2) {
        return name1 + "and" + name2;
    }

}
