package function;

public class Sequence1 {

    public static void main(String[] args) {
        int result = getSeqFirst5Diff3ByIndex(5);
        System.out.println(result);
    }
    
    /**
     * 初項5, 公差3の等差数列の指定項の数を返す.
     * @param seqtionNum 指定項.
     * @return 指定項の数.
     */
    public static int getSeqFirst5Diff3ByIndex(int seqtionNum) {
         return 5 + 3 * (seqtionNum - 1);
    }

}
