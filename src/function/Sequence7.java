package function;

import java.util.ArrayList;

public class Sequence7 {

    public static void main(String[] args) {
        System.out.println(getFibSeqStr());
        System.out.println(getFibSeqList());
    }
    
    /**
     * フィボナッチ数列の結果を返す(List使用).
     * @return 1000以下のフィボナッチ数列結果List
     */
    public static ArrayList<Integer> getFibSeqList() {
         ArrayList<Integer> seqList = new ArrayList<Integer>();
         int seqNum = 0;
         // 初項：0, 第2項：1
         for (int i = 0; seqNum <= 1000; i++) {
             if (i == 0) {
                 seqList.add(0);
             } else if (i == 1) {
                 seqList.add(1);
             } else {
                 if (seqList.get(i - 1) + seqList.get(i - 2) >= 1000) {
                     break;
                 }
                 seqList.add(seqList.get(i - 1) + seqList.get(i - 2));
             }
             seqNum = seqList.get(i);
         }
         return seqList;
         
    }
    
    /**
     * 項の数を返す（再帰使用）.
     * @param num 項数
     * @return 項の数
     */
    public static int getFibSeq(int num) {
        if (num <= 0) {
            return 0;
        } else if (num == 1 || num == 2) {
            return 1;
        } else {
            return getFibSeq(num - 1) + getFibSeq(num - 2);
        }
    }
    
    /**
     * 1000以下のフィボナッチ数列の結果を返す.
     * @return 項の数の文字列
     */
    public static String getFibSeqStr() {
        String fibStr = "";
        for (int i = 0; getFibSeq(i) <= 1000; i++) {
            fibStr += getFibSeq(i) + " ";
        }
        return fibStr;
    }
    

}
