package function;

public class Nabeatsu {
    public static void main(String[] args) {
        System.out.println(getNabeatsuStr(50));
    }
    
    /**
     * 指定数内で3の倍数と3のつく数の時にアホになる.
     * @param num 指定数
     * @return 文字列
     */
    public static String getNabeatsuStr(int num) {
        String nabeatsuStr = "";
        for (int i = 1; i <= num; i++) {
            if (i % 3 == 0 || String.valueOf(i).indexOf("3") >= 0) {
                nabeatsuStr += i + "(｡☉౪ ⊙｡)";
            } else {
                nabeatsuStr += i;
            }
            nabeatsuStr += " ";
        }
        return nabeatsuStr;
    }

}
