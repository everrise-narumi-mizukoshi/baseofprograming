package function;

public class Add {
    public static void main(String[] args) {
        int addResult = add(1, 3);
        System.out.println(addResult);
    }
    
    
    public static int add(int a, int b) {
        return a + b;
    }

}
