package function;

import java.util.ArrayList;

public class Sequence3 {

    public static void main(String[] args) {
        ArrayList<Integer> result = getAllSeqFirst5Diff3AsArray(5);
        System.out.println(result.toString());
    }
    
    /**
     * 初項5, 公差3の等差数列の指定した項までのすべての数を配列で返す.
     * @param sectionNum 指定項数.
     * @return すべての数の配列.
     */
    public static ArrayList<Integer> getAllSeqFirst5Diff3AsArray(int sectionNum) {
        ArrayList<Integer> allSeqResults = new ArrayList<Integer>();
        for (int i = 1; i <= sectionNum; i++) {
            allSeqResults.add(5 + 3 * (i - 1));
        }
         return allSeqResults;
    }

}
