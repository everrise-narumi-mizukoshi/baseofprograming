package function;

public class Sequence5 {

    public static void main(String[] args) {
        int result = getSeqNumsComDiff(3, 8, 3);
        System.out.println(result);
    }
    
    /**
     * 等差数列の結果を返す.
     * @param firstNum 初項
     * @param diffNum 公差
     * @param seqNum 指定項
     * @return 指定項の数
     */
    public static int getSeqNumsComDiff(int firstNum, int diffNum, int seqNum) {
         return firstNum + diffNum * (seqNum - 1);
    }
}
