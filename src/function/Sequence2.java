package function;

public class Sequence2 {

    public static void main(String[] args) {
        String result = getAllSeqFirst5Diff3AsString(5);
        System.out.println(result);
    }
    
    /**
     * 初項5, 公差3の等差数列の指定した項までのすべての数を文字列で返す.
     * @param sectionNum 指定項数.
     * @return すべての数のスペース区切りの文字列.
     */
    public static String getAllSeqFirst5Diff3AsString(int sectionNum) {
        String allSeqResult = "";
        for (int i = 1; i <= sectionNum; i++) {
            allSeqResult += 5 + 3 * (i - 1) + " ";
        }
         return allSeqResult;
    }

}
