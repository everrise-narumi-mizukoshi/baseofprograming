package function;

public class BaseOfMethod {
    /**
     * 関数の作成と実行.
     */
    public static void main(String[] args) {
        
        String voice = hello();
        System.out.println(voice);
    }
    
    public static String hello() {
        return "hello";
    }

}
