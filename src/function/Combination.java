package function;

public class Combination {
    /** 
     * 組み合わせ.
     */
    public static void main(String[] args) {
        String voice = hello(nameAndName("hogehoge", "piyopiyo"));
        System.out.println(voice);
    }
    
    public static String nameAndName(String name1, String name2) {
        return name1 + " and " + name2;
    }
    
    public static String hello(String name) {
        return "hello " + name;
    }

}
