package base;

public class Exercise {

    public static void main(String[] args) {
        // ワンライナー (1が表示される)
        System.out.println(true ? 1 : 2);
        
        // 多分岐 (four fiveが表示される)
        int a = 4;
        switch (a) {
        case 1:
            System.out.println("one");
            break;
        case 2:
            System.out.println("two");
            break;
        case 3:
            System.out.println("three");
            break;
        case 4:
            System.out.println("four");
        case 5:
            System.out.println("five");
            break;
        default:
            System.out.println("I don't know");
        }
        
        // あるある１ (1は表示される：aB = trueが代入されているから：= は代入と評価の意味合いを持つ)
        boolean aB = false;
        if (aB = true) {
            System.out.println("1");
        }
        
        // あるある２ (trueはboolean値でさらに予約語だから、代入ができないのはもちろん、変数として宣言もできない)
//        aB = false;
//        if (true = aB) {
//            System.out.println("1");
//        }
        
        // 世界のナベアツ
        for (int i = 1; i <= 50; i++) {
            if (i % 3 == 0 || String.valueOf(i).indexOf("3") >= 0) {
                System.out.println(i + "アホ");
            } else {
                System.out.println(i);
            }
        }
    }

}
