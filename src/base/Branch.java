package base;

public class Branch {
    
    /**
     * プログラミング基礎：分岐.
     */
    public static void main(String[] args) {
        // 基本１ (1が表示される)
        boolean a = true;
        if (a) {
            System.out.println("1");
        }
        
        // 基本２ (表示されない)
        a = false;
        if (a) {
            System.out.println("1");
        }
        
        // 2分岐１ (1が表示される)
        a = true;
        if (a) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 2分岐２ (2が表示される)
        a = false;
        if (a) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 同値比較１ (1が表示される)
        if (1 == 1) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 同値比較１ (2が表示される)
        if (1 == 0) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 大小比較１ (1が表示される)
        if (10 > 5) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 大小比較２ (2が表示される)
        if (10 < 5) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 変数の値の判定 (2が表示される)
        int aNum = 10;
        if (aNum < 5) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 変数同士の比較 (2が表示される)
        aNum = 10;
        int b = 5;
        if (aNum < b) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 否定１ (2が表示される)
        a = true;
        if (!a) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 否定２ (1が表示される)
        a = false;
        if (!a) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 判定の逆転 (2が表示される)
        if (!(1 == 1)) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 異なる判定 (2が表示される)
        if (1 != 1) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 複合条件１ (1が表示される)
        aNum = 5;
        if (aNum >= 5 && aNum <= 10) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 複合条件２ (2が表示される)
        aNum = 8;
        if (aNum == 5 || aNum == 10) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 絶対 (1が表示される)
        // || で必ずtrueになってしまうため、2を表示するのは不可能
        aNum = 8;
        if (aNum == 5 || true) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 分岐 (1が表示される)
        if (false == false) {
            System.out.println("1");
        } else {
            System.out.println("2");
        }
        
        // 複合条件分岐 (3が表示される)
        aNum = 5;
        if (aNum < 3) {
            System.out.println("1");
        } else if (aNum > 6) {
            System.out.println("2");
        } else {
            System.out.println("3");
        }
        
        // ネスト (2のみを表示するには、aNum >= 10 の値にする)
        aNum = 10;
        if (aNum < 10) {
            System.out.println("1");
        }
        System.out.println("2");
    }

}
