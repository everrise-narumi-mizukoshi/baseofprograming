package base;

public class Repeat {
    /**
     * プログラミング基礎：反復.
     */
    public static void main(String[] args) {
        // 基本 (10回実行される)
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
        
        // 連番１
        for (int i = 1; i <= 100; i++) {
            System.out.println(i);
        }
        
        // 連番２
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
        
        // 連番３
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }
        
        // 反復の制御 (if文内に入ると、for文はそこで止まる)
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            if (i > 5) {
                break;
            }
        }
        
        //  FizzBuzz１
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else {
                System.out.println(i);
            }
        }
        
        // FizzBuzz２
        int c = 0;
        for (int i = 1; i <= 100; i++) {
            if (i % 4 == 3) {  // cさんの順番の時
                if (i % 5 == 0 && i % 3 != 0) { // Buzzって言った時のみだから
                    c++;
                }
            }
        }
        System.out.println("CさんがBuzzといった回数：" + c);
    }

}
