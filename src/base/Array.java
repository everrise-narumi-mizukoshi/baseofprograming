package base;

public class Array {

    /**
     * プログラミング基礎：配列.
     */
    public static void main(String[] args) {
        // 作成と使用
        String a[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "z"};
        System.out.println(a[0]);
        System.out.println(a[1]);
        System.out.println(a[2]);
        System.out.println(a[3]);
        System.out.println(a[4]);
        System.out.println(a[5]);
        System.out.println(a[6]);
        System.out.println(a[7]);
        System.out.println(a[8]);
        System.out.println(a[9]);
        System.out.println(a[10]);
        
        // 反復
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }

}
