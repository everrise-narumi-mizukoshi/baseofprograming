package base;

public class Calculation {
    /**
     * プログラミング基礎：計算.
     */
    public static void main(String[] args) {
        // リテラル
        System.out.println(1 + 1);
        
        // 和
        int a = 1;
        int b = 2;
        System.out.println(a + b);
        
        // 差
        a = 10;
        b = 6;
        System.out.println(a - b);
        
        // 積
        a = 9;
        b = 9;
        System.out.println(a * b);
        
        // 商
        a = 10;
        b = 2;
        System.out.println(a / b);
        
        // 剰余
        a = 10;
        b = 3;
        System.out.println(a % b);
        
        // 結合１ (*/, +- の順番で計算する)
        System.out.println(10 + 1000 * 40 / 2 - 45);
        
        // 結合２ (カッコ内から計算して*/, +-の順番で計算する)
        System.out.println((10 + 1000) * 40 / 2 - 45);
        
        // 計算と代入
        a = 1;
        b = 2;
        a = b;
        b = 6;
        a = b + 1;
        a = a + 2;
        a = a + 3;
        a += 10;
        System.out.println(a);
    }

}
