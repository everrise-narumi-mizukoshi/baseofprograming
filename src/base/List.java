package base;

import java.util.HashMap;
import java.util.Map;

public class List {
    /**
     * プログラミング基礎：リスト
     */
    public static void main(String[] args) {
        // 連想配列１
        Map<String, String> a = new HashMap<String, String>();
        a.put("a", "A");
        a.put("b", "B");
        a.put("c", "C");
        a.put("d", "D");
        a.put("e", "E");
        a.put("f", "F");
        a.put("g", "G");
        a.put("h", "H");
        a.put("i", "I");
        a.put("j", "J");
        a.put("z", "Z");
        System.out.println(a.get("e"));
        System.out.println(a.get("b"));
        System.out.println(a.get("z"));
        
        // 連想配列２
        Map<String, String> prefectures = new HashMap<String, String>();
        prefectures.put("JP-01", "北海道");
        prefectures.put("JP-02", "青森県");
        prefectures.put("JP-03", "岩手県");
        prefectures.put("JP-04", "宮城県");
        prefectures.put("JP-05", "秋田県");
        prefectures.put("JP-06", "山形県");
        prefectures.put("JP-07", "福島県");
        prefectures.put("JP-08", "茨城県");
        prefectures.put("JP-09", "栃木県");
        prefectures.put("JP-10", "群馬県");
        prefectures.put("JP-11", "埼玉県");
        prefectures.put("JP-12", "千葉県");
        prefectures.put("JP-13", "東京都");
        prefectures.put("JP-14", "神奈川県");
        prefectures.put("JP-15", "新潟県");
        prefectures.put("JP-16", "富山県");
        prefectures.put("JP-17", "石川県");
        prefectures.put("JP-18", "福井県");
        prefectures.put("JP-19", "山梨県");
        prefectures.put("JP-20", "長野県");
        prefectures.put("JP-21", "岐阜県");
        prefectures.put("JP-22", "静岡県");
        prefectures.put("JP-23", "愛知県");
        prefectures.put("JP-24", "三重県");
        prefectures.put("JP-25", "滋賀県");
        prefectures.put("JP-26", "京都府");
        prefectures.put("JP-27", "大阪府");
        prefectures.put("JP-28", "兵庫県");
        prefectures.put("JP-29", "奈良県");
        prefectures.put("JP-30", "和歌山県");
        prefectures.put("JP-31", "鳥取県");
        prefectures.put("JP-32", "島根県");
        prefectures.put("JP-33", "岡山県");
        prefectures.put("JP-34", "広島県");
        prefectures.put("JP-35", "山口県");
        prefectures.put("JP-36", "徳島県");
        prefectures.put("JP-37", "香川県");
        prefectures.put("JP-38", "愛媛県");
        prefectures.put("JP-39", "高知県");
        prefectures.put("JP-40", "福岡県");
        prefectures.put("JP-41", "佐賀県");
        prefectures.put("JP-42", "長崎県");
        prefectures.put("JP-43", "熊本県");
        prefectures.put("JP-44", "大分県");
        prefectures.put("JP-45", "宮崎県");
        prefectures.put("JP-46", "鹿児島県");
        prefectures.put("JP-47", "沖縄県");
        
        System.out.println(prefectures.get("JP-17"));
        
        // 連想配列３
        Map<String, Map<String, String>> prefectures2 = new HashMap<String, Map<String, String>>();
        Map<String, String> hokkaido = new HashMap<String, String>();
        hokkaido.put("kanji", "北海道");
        hokkaido.put("hiragana", "ほっかいどう");
        hokkaido.put("katakana", "ホッカイドウ");
        prefectures2.put("JP-01", hokkaido);
        
        Map<String, String> aomori = new HashMap<String, String>();
        aomori.put("kanji", "青森県");
        aomori.put("hiragana", "あおもりけん");
        aomori.put("katakana", "アオモリケン");
        prefectures2.put("JP-02", aomori);
        
        Map<String, String> ishikawa = new HashMap<String, String>();
        ishikawa.put("kanji", "石川県");
        ishikawa.put("hiragana", "いしかわけん");
        ishikawa.put("katakana", "イシカワケン");
        prefectures2.put("JP-17", ishikawa);
        
        Map<String, String> okinawa = new HashMap<String, String>();
        okinawa.put("kanji", "沖縄県");
        okinawa.put("hiragana", "おきなわけん");
        okinawa.put("katakana", "オキナワケン");
        prefectures2.put("JP-47", okinawa);
        
        System.out.println(prefectures2.get("JP-17").get("hiragana"));
    }

}
