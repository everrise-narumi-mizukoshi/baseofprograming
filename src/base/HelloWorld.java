package base;

public class HelloWorld {

    /**
     * プログラミング基礎：出力、変数.
     */
    public static void main(String[] args) {
        // 表示（display）
        System.out.println("Hello World");
        
        // 順次（in order）
        System.out.println("Hello World");
        System.out.println("Hello EVERRISE");
        
        // 代入・使用（assignment）
        String a = "Hello String";
        System.out.println(a);
        
        // 代入・使用２（assignment2）
        a = "Hello myname";
        System.out.println(a);
        
        // 交換（swap）
        a = "A";
        String b = "B";
        
        String c = a;
        a = b;
        b = c;
        System.out.println(a);
        System.out.println(c);
    }

}
